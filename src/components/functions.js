import axios from 'axios';

const ymaps = window.ymaps

export function init() {
	document.getElementById('map').innerHTML = ''

	const params = new URLSearchParams(window.location.hash)

	let balloonContainer
	const myMap = new ymaps.Map('map', {
		center: params.get('ll') ? params.get('ll').split(',') : [
			55.753994,
			37.622093],
		zoom: params.get('z') ? params.get('z') : 9,
	}, {
		searchControlProvider: 'yandex#search',
	});

	myMap.events.add('click', async function(e) {
		let coords = e.get('coords');

		const weather = await getWeather(coords)
		balloonContainer = myMap.balloon.open(coords, weather)
		myMap.geoObjects.add(balloonContainer);

	});

	let centerURL, zoomURL

	myMap.events.add('boundschange', function(e) {

		centerURL = e.get('newCenter')
		zoomURL = e.get('newZoom')

		document.location.hash = '&ll=' + centerURL.toString() + '&z=' +
				zoomURL.toString()
	})

	async function getWeather(coords) {
		const res = await axios.get(
				`https://weather-app-457-server.herokuapp.com/requestWeather?lat=${coords[0]}&lon=${coords[1]}`)

		if (res.data.result === 'error') {
			console.log('Возникла ошибка: ' + res.data.message)
		}

		const fact = res.data.data.fact

		return `
${fact.condition ? `<div>${{
			'clear': 'Ясно',
			'partly-cloudy': 'Малооблачно',
			'cloudy': 'Облачно с прояснениями',
			'overcast': 'Пасмурно',
			'drizzle': 'Морось',
			'light-rain': 'Небольшой дождь',
			'rain': 'Дождь',
			'moderate-rain': 'Умеренно сильный дождь',
			'heavy-rain': 'Сильный дождь',
			'continuous-heavy-rain': 'Длительный сильный дождь',
			'showers': 'Ливень',
			'wet-snow': 'Дождь со снегом',
			'light-snow': 'Небольшой снег',
			'snow': 'Снег',
			'snow-showers': 'Снегопад',
			'hail': 'Град',
			'thunderstorm': 'Гроза',
			'thunderstorm-with-rain': 'Дождь с грозой',
			'thunderstorm-with-hail': 'Гроза с градом',
		}[fact.condition]}</div>` : ''}
		${fact.phenom_condition
				? `<div>${{
					'fog': 'Туман',
					'mist': 'Дымка',
					'smoke': 'Смог',
					'dust': 'Пыль',
					'dust-suspension': 'Пылевая взвесь',
					'duststorm': 'Пыльная буря',
					'thunderstorm-with-duststorm': 'Пыльная буря с грозой',
					'drifting-snow': 'Слабая метель',
					'blowing-snow': 'Метель',
					'ice-pellets': 'Ледяная крупа',
					'freezing-rain': 'Ледяной дождь',
					'tornado': 'Торнадо',
					'volcanic-ash': 'Вулканический пепел',
				}[fact.phenom_condition]}</div>`
				: ''}
${fact.temp ? `<div>Температура: ${fact.temp + '&deg;'}</div>` : ''}
${fact.humidity ? `<div>Влажность: ${fact.humidity + '%'}</div>` : ''}
${fact.feels_like ? `<div>Ощущается: ${fact.feels_like + '&deg;'}</div>` : ''}

${fact.temp_water
				? `<div>Температура воды: ${fact.temp_water + '&deg;'}</div>`
				: ''}
${fact.wind_speed
				? `<div>Скорость ветра: ${fact.wind_speed + ' м/с'}</div>`
				: ''}
${fact.wind_gust
				? `<div>Скорость порывов: ${fact.wind_gust + ' м/с'}</div>`
				: ''}
${fact.wind_dir
				? `<div>Направление ветра: ${{
					'nw': 'северо-западное',
					'n': 'северное',
					'ne': 'северо-восточное',
					'e': 'восточное',
					'se': 'юго-восточное',
					's': 'южное',
					'sw': 'юго-западное',
					'w': 'западное',
					'с': 'штиль',
				}[fact.wind_dir]}</div>`
				: ''}
${fact.pressure_mm ? `<div>Давление: ${fact.pressure_mm} мм рт. ст.</div>` : ''}
${fact.pressure_pa
				? `<div>Давление: ${fact.pressure_pa} гпа</div>`
				: ''}
`
	}
}


