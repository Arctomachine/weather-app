import React, {useEffect} from 'react';
import {init} from './functions';

function Map(props) {

	useEffect(() => {
		window.ymaps.ready(init);
	}, [])

	useEffect(() => {

	}, [])

	return (
			<div id="map" style={{
				width: 640,
				height: 480,
			}}></div>
	);
}

export default Map;
