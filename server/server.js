const express = require('express')
const cors = require('cors')
const axios = require('axios')
require('dotenv').config()
const app = express()
const PORT = 3001

app.use(cors())

app.get('/requestWeather', async function(req, res, next) {

	if (!req?.query?.lat || !req?.query?.lon) {
		return res.json({
			result: 'error',
			message: 'Не заданы координаты',
		})
	}

	const apiCall = await axios.get(
			`https://api.weather.yandex.ru/v2/forecast?lat=${req.query.lat}&lon=${req.query.lon}&limit=1`,
			{
				headers: {
					'X-Yandex-API-Key': process.env.WEATHER_KEY,
				},
			})

	if (apiCall.status !== 200) {
		return res.json({
			result: 'error',
			message: 'Ошибка запроса к сервису',
		})
	}

	res.json({
		result: 'success',
		data: apiCall.data,
	})
})

app.listen(PORT, function() {
	console.log('server ', PORT)
})
